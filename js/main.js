const prop = ( data, name ) => data.map( item => item[ name ] ),
  summ = data => data.reduce(( total, value ) => total + value, 0 );

class SpriteGenerator {
  constructor( container ) {
    this.uploadButton = container.querySelector( '.sprite-generator__upload' );
    this.submitButton = container.querySelector( '.sprite-generator__generate' );
    this.imagesCountContainer = container.querySelector( '.images__added-count-value' );
    this.codeContainer = container.querySelector( '.sprite-generator__code' );
    this.imageElement = container.querySelector( '.sprite-generator__result-image' );
    this.images = [];
    this.imagesCount = 0;
    this.allImagesWidth = 0;
    this.maxImagesHeight = 0;
    this.registerEvents();
  }

  registerEvents() {
    this.uploadButton.setAttribute('accept', 'image/*');
    this.uploadButton.addEventListener('change', this.pushImages.bind(this));
    this.submitButton.addEventListener('click', this.spriteGenerate.bind(this));
  }

  pushImages( event ) {
    for (let item of Array.from(event.currentTarget.files)) {
      this.images.push(item);
    }
    this.imagesCount = this.images.length;
    this.imagesCountContainer.textContent = this.imagesCount;
  }

  spriteGenerate() {
    const arrayForCanvas = [];

    return new Promise(resolve => {
      for (let image of this.images) {
        let img = document.createElement('img');
        img.src = URL.createObjectURL(image);
        img.addEventListener('load', event => {
          arrayForCanvas.push(img);
          URL.revokeObjectURL(img.src);
          if (this.images.indexOf(image) === this.images.length - 1) {
            resolve(arrayForCanvas);
          }
        });
        document.querySelector('.sprite-generator__preview').appendChild(img);
      }
    })
      .then( arrayForCanvas => {
        this.getSpriteSizes(arrayForCanvas);
        return arrayForCanvas;
      })
      .then( arrayForCanvas => { 
        this.getCSS(arrayForCanvas);
        return arrayForCanvas;
      })   
      .then( arrayForCanvas => { 
        this.canvasCreate(arrayForCanvas); 
      });
  }

  getSpriteSizes( arrayForCanvas ) {
    arrayForCanvas.forEach((item) => {
      this.allImagesWidth += item.offsetWidth;
      this.maxImagesHeight = Math.max(this.maxImagesHeight, item.offsetHeight);
    });
  }

  getCSS( arrayForCanvas ) {
    this.codeContainer.value = '.icon \{\n \tdisplay: inline-block\;\n \tbackground-image: url\(img/sprite.png\)\;\n \}\n';
    let posX = 0;
    arrayForCanvas.forEach((item) => {
      this.codeContainer.value += `.icon_${arrayForCanvas.indexOf(item) + 1} \{\n \tbackground-position: -${posX}px 0\;\n \twidth: ${item.offsetWidth}px\;\n \theight: ${item.offsetHeight}px\;\n \}\n`;
      posX += item.offsetWidth;
    });
  }

  canvasCreate( arrayForCanvas ) {
    let x = 0;
    const canvas = document.createElement('canvas'),
          ctx = canvas.getContext('2d');
    canvas.width = this.allImagesWidth;
    canvas.height = this.maxImagesHeight;

    for (let image of arrayForCanvas) {
      ctx.drawImage(image, x, 0);
      x += image.offsetWidth;
    }

    this.getFinalImage(canvas);
  }

  getFinalImage( canvas ) {
    let finalImage = document.createElement('img');
    finalImage.src = canvas.toDataURL();
    document.querySelector('.sprite-generator__preview').innerHTML = '';
    document.querySelector('.sprite-generator__preview').appendChild(finalImage); 
  }
}

new SpriteGenerator( document.getElementById( 'generator' ));